﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LV2_Controller : MonoBehaviour
{

    private float coin = 0;
    private float key = 0;
    private float health = 3;

    // Am thanh
    [SerializeField]
    private AudioSource audioSource;
    [SerializeField]
    private AudioClip getCoinClip, touchEnemyClip, diedClip;
    public TextMeshProUGUI textCoins, healthTMP;

    [SerializeField]
    private GameObject pausePanel, gameOverPanel;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // nhặt tiền
        if (collision.transform.tag == "Coin")
        {
            coin++;
            textCoins.text = coin.ToString();
            Destroy(collision.gameObject);
            audioSource.PlayOneShot(getCoinClip);
        }

        // Nhặt key
        if (collision.transform.tag == "Key")
        {
            key++;
            Destroy(collision.gameObject);
            audioSource.PlayOneShot(getCoinClip);
        }

        // Chuyển màn
        if (collision.transform.tag == "CompleteLevel" && key == 1)
        {
            Application.LoadLevel("Level3_Scene");
        }

        // Chạm vào gai
        if (collision.transform.tag == "Enemy")
        {
            if (health > 0)
            {
                health--;
                healthTMP.text = health.ToString();
                audioSource.PlayOneShot(touchEnemyClip);
            }
            else
            {
                Time.timeScale = 0;
                gameOverPanel.SetActive(true);
                audioSource.PlayOneShot(diedClip);
            }
        }

        // Ăn thêm mạng
        if (collision.transform.tag == "morehealthy")
        {
            health++;
            healthTMP.text = health.ToString();
            Destroy(collision.gameObject);
        }
    }


    public void _PauseButton()
    {
        Time.timeScale = 0;
        pausePanel.SetActive(true);
    }

    public void _ResumeButton()
    {
        Time.timeScale = 1;
        pausePanel.SetActive(false);
    }

    public void _MenuButton()
    {
        Time.timeScale = 1;
        Application.LoadLevel("MainMenu");
    }

    public void _RestartGameButton()
    {
        Time.timeScale = 1;
        Application.LoadLevel("Level1_Scene");
        // Application.LoadLevel(Application.loadedLevel); danh cho game co nhieu Map, level(That level Again)
    }

}
