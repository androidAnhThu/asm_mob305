﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public Transform target;
    private Transform playertransform;
    public float offset;
    public float smoothDampTime = 0.15f;
    private Vector3 smoothDampVelocity = Vector3.zero;
    public Transform leftBounds;
    public Transform rightBounds;
    private float levelMinX, levelMaxX;
    void Start()
    {
        playertransform = GameObject.FindGameObjectWithTag("Player").transform;
        float leftBoundsWidth = leftBounds.GetComponentInChildren<SpriteRenderer>().bounds.size.x / 2;
        float rightBoundsWidth = rightBounds.GetComponentInChildren<SpriteRenderer>().bounds.size.x / 2;


        levelMinX = leftBounds.position.x + leftBoundsWidth;
        levelMaxX = rightBounds.position.x - rightBoundsWidth;

    }
    void Update()
    {
        if (target)
        {
            float targetX = Mathf.Max(levelMinX, Mathf.Min(levelMaxX, target.position.x));
            float x = Mathf.SmoothDamp(transform.position.x, targetX, ref smoothDampVelocity.x, smoothDampTime);
            transform.position = new Vector3(x, transform.position.y, transform.position.z);
        }
    }
    void LateUpdate()
    {
        Vector3 temp = transform.position;

        temp.x = playertransform.position.x;
        temp.y = playertransform.position.y;

        temp.y += offset;
        temp.x += offset;

        transform.position = temp;

    }
}

